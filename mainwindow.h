#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QAbstractButton>
#include <QKeyEvent>
#include <cmath>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void numberGroup_clicked(QAbstractButton*);
    void actionGroup_clicked(QAbstractButton*);

    void on_actionDel_clicked();
    void on_actionCalc_clicked();
    void on_comma_clicked();
    void on_actionClear_clicked();
    void on_actionPercent_clicked();
    void on_actionSign_clicked();
    //math
    void on_actionSin_clicked();
    void on_actionCos_clicked();
    void on_actionTg_clicked();
    void on_actionCtg_clicked();
    void on_actionSquare_clicked();
    void on_actionSqrt_clicked();
    //logs
    void on_actionLog_clicked();
    void on_actionLn_clicked();
    //valute
    void on_actionEur_clicked();
    void on_actionDol_clicked();

private:
    Ui::MainWindow *ui;
    //Digit limit
    const int DIGIT_LIMIT = 16;
    //Flag to check whether the previous button that was clicked was an operator
    bool operatorClicked;
    //Last operator requested
    QChar storedOperator;
    //Flag to check whether a number is stored in memory
    bool hasStoredNumber;
    //Stored number
    double storedNumber;
    //Flag to prevent from double conversion to valute (0 = free; 1 = eur; 2 = dol)
    int wasValuteCalc;
    //Flag to clear display to new input after calculations
    bool calcCompleted;
    //Calculate result based on stored number and displayed number
    void calculate_result();

protected:
    void keyPressEvent(QKeyEvent *e);
};

#endif // MAINWINDOW_H
